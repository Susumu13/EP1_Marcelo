#include "network.hpp"
#include "array.hpp"
#include "crypto.hpp"
#include <iostream>
#include <fstream>
#include <stdio.h>

using namespace std;

int main() {
	int teste = network::connect("45.55.185.4",3000);
	// Request and reception
	array::array * request = array::create(7);
	request->data[0] = 0x03;
	request->data[1] = 0;
   	request->data[2] = 0;
   	request->data[3] = 0;
  	request->data[4] = 0xC0;
  	request->data[5] = 0;
  	request->data[6] = 0;
	array::array * start;
	// end

	// Envio Protocolo de Registro
		
	array::array *valor_dados = array::create(8);
	int tamanho, i;	

	RSA * serverpk = crypto::rsa_read_public_key_from_PEM("server_pk.pem");
	RSA * clientesk = crypto::rsa_read_private_key_from_PEM("client_sk.pem");
	byte valor[] = {0x6E,0x5D,0x53,0x25,0xEF,0x25,0x20,0x90};

	memcpy(valor_dados->data, valor, 8); 
	
	array::array * idCripto = crypto::rsa_encrypt(valor_dados , serverpk); 
  
	for (i=0;i < ((int) idCripto->length);i++) {}
	cout << *idCripto << endl;
	tamanho = 23 + i;
	array::array *pacote = array::create(tamanho);
	pacote->data[0] = 0xC2;
   	pacote->data[1] = 0x00;
   	pacote->data[2] = 0x02;	
   	memcpy(pacote->data + 3, idCripto,i);

	array::array* hash = crypto::sha1(idCripto);

	memcpy(pacote->data + 3 + i, hash->data, 20);

	array::array * registerId = array::create(4 + tamanho);
   	registerId->data[0] = 0x04;
  	registerId->data[1] = 0x02;
   	registerId->data[2] = 0;
   	registerId->data[3] = 0;

	memcpy(registerId->data +4, pacote->data, tamanho);
	cout << tamanho << endl;
	cout << *registerId << endl;
	//end
/*
	// Envio Protocolo de Autenticação
	array::array * request_auth = array::create(35);
	byte auth[] = {31,0x00,0x00,0x00,0xA0,0x08,0x0};
	memcpy(request_auth->data, auth,7);
	memcpy(request_auth->data + 3, idCripto,sizeof(seguro));
	memcpy(request_auth->data + 3 +sizeof(idCripto), hash->data, 20);
	
	array::array * request_challenge = array::create(7);
	byte challenge[] = {3,0x00,0x00,0x00,0xA2,0x0,0x0};
	memcpy(request_challenge->data, challenge,7);
	
	array::array * authenticate = array::create(35);
	byte authenticate[] = {31,0x00,0x00,0x00,0xA5,0x08,0x0};
// precisa mudar
	memcpy(request_authenticate->data, authenticate,7);
	memcpy(request_authenticate->data + 3, idCripto,sizeof(seguro));
	memcpy(request_authenticate->data + 3 +sizeof(idCripto), hash->data, 20);
	
	//end



	// Protocolo de Requisição
	array::array * request_object = array::create(35);
	byte object[] = {31,0x00,0x00,0x00,0xB0,0x08,0x0};
	memcpy(request_auth->data, object,7);
//precisa mudar
	memcpy(request_auth->data + 3, idCripto,sizeof(seguro));
	memcpy(request_auth->data + 3 +sizeof(idCripto), hash->data, 20);
	

	//end
*/
	
	array::destroy(pacote);
	array::destroy(valor_dados);
	array::destroy(idCripto);
	array::destroy(hash);
 	if(teste < 0)
	{
		cout << "Falha na conexao" << endl;	
		return 1;	
	}	
	else
	{
		network::write(teste,request);
		start = network::read(teste);
		if (pacote == nullptr)
		{
			cout << "Nao ha leitura" << endl;		
		}		
		else {
			cout << "Imprimindo conteudo do pacote recebido [" << start->length << "]" << endl;
			cout << *start << endl;
		}
		array::destroy(request);
		array::destroy(start);
		// Recebimento do request_start
		
		network::write(teste,registerId);
		array::destroy(registerId);
		
		array::array * registered = network::read(teste);
		cout << *registered << endl;

		array::array* chave = crypto::rsa_decrypt(registered,clientesk);
		
		cout << *chave << endl;
		
		array::destroy(registered);
		array::destroy(chave);
		
	}
	network::close(teste);
	
	crypto::rsa_destroy_key(clientesk);
	crypto::rsa_destroy_key(serverpk);	
	return 0;
}
